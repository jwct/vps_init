#!/bin/bash

ADMIN_USER=myadmin
DEV_USER=mydev
PWD_DEFULT=Change
SSH_PORT=30022

function step00_basic()
{
    echo =======================================
    echo "Task : Update timezone"
    unlink /etc/localtime 
    ln -s /usr/share/zoneinfo/Hongkong /etc/localtime
    date

    echo =======================================
    echo "Task : Add Admin user"
    useradd ${ADMIN_USER}
    echo ${ADMIN_USER}:${ADMIN_USER}!!!${PWD_DEFULT}| chpasswd 
    usermod -aG wheel ${ADMIN_USER}
    chage -d 0 ${ADMIN_USER}
    groups ${ADMIN_USER}

    echo =======================================
    echo "Task : Add Dev user"
    useradd ${DEV_USER}
    echo ${DEV_USER}:${DEV_USER}!!!${PWD_DEFULT}| chpasswd 
    chage -d 0 ${DEV_USER}
    groups ${DEV_USER}

    echo =======================================
    echo "Task : Update packages"
    dnf update -y

    echo "Task : Install packages "  
    dnf  install bind-utils -y
    dnf install epel-release -y
}
function step01_security()
{
    # install_openvpn
    config_ssh
    config_firewall
}

function step02_dev()
{
    install_git
    # install_emacs
    install_docker
    # install_gui
}

function step03_postcreate()
{
    dnf clean all
    
    # echo =====================
    # echo "Change openvpn password"
	# passwd openvpn 
	
    echo =====================
    echo "Change root password"
    passwd 

    echo =====================
    echo "Change GRUB password"
    grub2-setpassword
    
    echo =====================
    echo "disable root login"
    usermod -s /sbin/nologin root
    grep root /etc/passwd

}

function config_ssh()
{
    echo =======================================
    echo "Task : Change SSH port"

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original
    cat > /tmp/sshd_config <<EOL
PermitRootLogin no
Port ${SSH_PORT} 
AllowUsers ${DEV_USER}
EOL
    cat /tmp/sshd_config >> /etc/ssh/sshd_config 
    diff /etc/ssh/sshd_config /etc/ssh/sshd_config.original
    
    service sshd restart
}

function config_firewall()
{
    echo =======================================
    echo "Task : Remove default ssh port"
    firewall-cmd --remove-service=ssh --permanent
    firewall-cmd --add-port=443/tcp  --permanent
    firewall-cmd --add-port=${SSH_PORT}/tcp  --permanent
    firewall-cmd --reload
    firewall-cmd --list-all
}

function install_openvpn()
{
    echo =======================================
    echo "Task : install openvpn"
    dnf -y install https://as-repository.openvpn.net/as-repo-centos8.rpm
    dnf -y install openvpn-as
}

function install_git() {
    echo =======================================
    echo "Task : Install Git Client"
    dnf install git-all -y
}

function install_emacs()
{
    dnf install emacs -y
    # download config
    mkdir /tmp/emacs
    curl -o /tmp/emacs/my.emacs https://gitlab.com/jwct/vps_init/raw/master/emacs/my.emacs
    curl -o /tmp/emacs/dired-details.el https://www.emacswiki.org/emacs/download/dired-details.el

    chmod -R 666 /tmp/emacs
}

function install_docker()
{
    echo =======================================
    echo "Task : Install docker"
    yum install -y yum-utils
	yum config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    yum install docker-ce docker-ce-cli containerd.io -y --allowerasing

    firewall-cmd --zone=trusted --change-interface=docker0  --permanent
    firewall-cmd --zone=trusted --add-masquerade --permanent
    
    curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

    systemctl start docker
    systemctl enable docker

    usermod -aG docker ${DEV_USER}
    groups ${DEV_USER}

}

function install_gui()
{
    dnf groupinstall "Server with GUI" -y

    #systemctl set-default graphical
}

step00_basic
step01_security
step02_dev
step03_postcreate