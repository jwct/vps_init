docker run -it --rm --name certbot \
       -p 80:80 \
       -v "`pwd`/letsencrypt:/etc/letsencrypt" \
       -v "`pwd`/lib/letsencrypt:/var/lib/letsencrypt" \
       certbot/certbot certonly
