
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq make-backup-files nil) ; disable backup file
(setq auto-save-default nil) ; stop auto backup
(defun my-split ()
  (interactive)
  (split-window-horizontally)
) ; default slip windows function

(defun my-enlarge-h ()
  (interactive)
    (enlarge-window 30 t)
) ; enlarge windows by 30

; add dired detail hide 
(add-to-list 'load-path "~/.emacs.d/lisp/")
(require 'dired-details)
(dired-details-install)


; add hook to redraw isearch result
; (add-hook 'isearch-update-post-hook 'redraw-display)

; bind *.js to js2-mode
; if js2-mode is not install , install it by 
; M-x package-install RET js2-mode RET

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(js-indent-level 2)
 '(package-selected-packages (quote (js2-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(setq-default js2-basic-offset 2)
