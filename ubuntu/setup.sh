#!/bin/bash

ADMIN_USER=myadmin
DEV_USER=mydev
PWD_DEFULT=Change
SSH_PORT=30022

function step00_basic()
{
    echo =======================================
    echo "Task : Update timezone"
    unlink /etc/localtime 
    ln -s /usr/share/zoneinfo/Hongkong /etc/localtime
    date

    echo =======================================
    echo "Task : Add Admin user"
    useradd -s /bin/bash -m ${ADMIN_USER}
    echo ${ADMIN_USER}:${ADMIN_USER}...${PWD_DEFULT}| chpasswd 
    usermod -aG sudo ${ADMIN_USER}
    chage -d 0 ${ADMIN_USER}
    groups ${ADMIN_USER}

    echo =======================================
    echo "Task : Add Dev user"
    useradd -s /bin/bash -m ${DEV_USER}
    echo ${DEV_USER}:${DEV_USER}...${PWD_DEFULT}| chpasswd 
    chage -d 0 ${DEV_USER}
    groups ${DEV_USER}

    echo =======================================
    echo "Task : Update packages"
    apt-get update

}
function step01_security()
{
    config_ssh
}

function step02_dev()
{
    
    install_emacs
    install_docker
}

function step03_postcreate()
{
    apt-get clean 
    
    echo =====================
    echo "Change root password"
    passwd 
    
    echo =====================
    echo "disable root login"
    usermod -s /sbin/nologin root
    grep root /etc/passwd

}

function config_ssh()
{
    echo =======================================
    echo "Task : Change SSH port"

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original
	
	sed -i 's/^PermitRootLogin yes/#PermitRootLogin yes/g' /etc/ssh/sshd_config
	
    cat > /tmp/sshd_config <<EOL
PermitRootLogin no
Port ${SSH_PORT} 
AllowUsers ${DEV_USER}
EOL
    cat /tmp/sshd_config >> /etc/ssh/sshd_config 
    diff /etc/ssh/sshd_config /etc/ssh/sshd_config.original
    
    service sshd restart
}


function install_emacs()
{
    apt install emacs-nox -y
    # download config
    mkdir /tmp/emacs
    curl -o /tmp/emacs/my.emacs https://gitlab.com/jwct/vps_init/raw/master/emacs/my.emacs
    curl -o /tmp/emacs/dired-details.el https://www.emacswiki.org/emacs/download/dired-details.el

    chmod -R 666 /tmp/emacs
}

function install_docker()
{
    echo =======================================
    echo "Task : Install docker"
	apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
	-y
	
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
	
	echo \
	"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
	$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
	
	apt-get update
	apt-get install docker-ce docker-ce-cli containerd.io -y
	
    systemctl start docker
    systemctl enable docker

    usermod -aG docker ${DEV_USER}
    groups ${DEV_USER}

}

step00_basic
step01_security
step02_dev
step03_postcreate
